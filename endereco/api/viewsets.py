from rest_framework import viewsets
from endereco.models import Endereco
from .serializer import EnderecoSerializer


class EnderecoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Endereco.objects.all()
    serializer_class = EnderecoSerializer
