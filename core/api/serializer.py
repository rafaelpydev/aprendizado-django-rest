from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from core.models import PontoTuristico
from atracoes.api.serializer import AtracaoSerializer
from endereco.api.serializer import EnderecoSerializer
from comentarios.api.serializer import ComentarioSerializer
from avaliacoes.api.serializer import AvaliacaoSerializer


class PontoTuristicoSerializer(ModelSerializer):
    atracao = AtracaoSerializer(many=True) # Relação ManyToMany
    endereco = EnderecoSerializer()
    comentario = ComentarioSerializer(many=True)
    avaliacao = AvaliacaoSerializer(many=True)
    descricao_completa = SerializerMethodField()

    class Meta:
        model = PontoTuristico
        fields = ('id', 'nome', 'descricao', 'aprovado', 'foto', 'atracao',
                  'comentario', 'avaliacao', 'endereco', 'descricao_completa',
                  'descricao_completa2')

    def get_descricao_completa(self, obj):
        return '{} - {}'.format(obj.nome, obj.descricao)
