from rest_framework import viewsets
from comentarios.models import Comentario
from .serializer import ComentarioSerializer


class ComentarioViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # queryset = Comentario.objects.all()
    serializer_class = ComentarioSerializer

    def get_queryset(self):
        return Comentario.objects.filter(aprovado=True)
